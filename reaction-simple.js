var debouncetime = 100;
console.log("Press when you are ready!");
var inittime = 0;
setWatch(function() {
  var presstime = getTime();
  var result = presstime - inittime - (debouncetime/1000);
  if (inittime !== 0)
    console.log("You needed " + result + " seconds.");
  LED1.reset();
  setTimeout(function() {
    LED1.set();
    inittime = getTime();
  }, (3+Math.random()*5)*1000);
}, BTN, {edge:"rising", debounce:debouncetime, repeat:true});