# Reaction Game for Puck.js

This JavaScript code can be used at https://www.espruino.com/ide/. Copy the code in the editor area and upload the code. As soon as the upload is finied, follow the instructions in the command line.

### Warning

Please use an external button, if you use this game not only for testing purposes. The button can be damaged!

To do this, connect one wire to "3V" and the other to "D2". Add this line to the beginning of the scripts:

```javascript
pinMode(D2, "input_pulldown");
```

To use the external button, change the following line and replace "BTN" with "D2".

```javascript
}, D2, {edge:"rising", debounce:debouncetime, repeat:true});
```

But do not get to much addicted ;)

## Simple game

As soon as the red led starts to lighten up, press the button and it measures your reaction time. Do it again and again...

## Standard game

In addition to the standard game, the green led will lighten up some times. DO NOT press the button, wait a second and press it to continue. If you want to stop the game, make an click where you are not suppost to click

## Advanced game

Also the blue led will lighen up, double click to be successful. The rest is like in the other games.

Enjoy!