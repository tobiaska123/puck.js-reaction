// measure time start
var inittime = 0;
// measure time press
var presstime = 0;
// measure time result
var resulttime = 0;
// debounce time
var debouncetime = 50;
// game has started
var ready = 0;
// do not continue the game
var end = 0;
// active led
var led = 0;
// interrupt to stop timer
var stoptimeout = 0;
// rand to select led
var rand = 0;
// double press inicator
var doublepress = 0;
LED1.reset(); //red
LED2.reset(); //green
LED3.reset(); //blue

setWatch(function() {
  presstime = getTime();
  resulttime = presstime - inittime - debouncetime/1000;

  if (end == 1) {
    end = 0;
    inittime = 0;
    ready = 0;
    led = 0;
  }

  if (inittime !== 0 && ready === 0) {
    console.log("Game over - pressed twice!");
    if (led == 1)
      LED1.reset();
    if (led == 2)
      LED2.reset();
    if (led == 3)
      LED3.reset();
    end = 1;
  }
  if (inittime !== 0 && ready == 1 && led == 1)
    console.log("It took you " + resulttime + " seconds.");
  if (inittime !== 0 && ready == 1 && led == 2) {
    console.log("Game over - pressed when you should not!");
    clearInterval(stoptimeout);
    end = 1;
  }

  if (inittime !== 0 && ready == 1 && led == 3) {
    if (doublepress === 0)
      doublepress = 1;
    else {
      console.log("It took you " + resulttime + " seconds.");
      doublepress = 0;
    }
  }

  if (led == 1)
    LED1.reset();
  if (led == 2)
    LED2.reset();
  if (led == 3)
    LED3.reset();
  if (doublepress === 0)
    ready = 0;

  if (end === 0 && doublepress === 0) {
    setTimeout(function() {
      if (end === 0) {
        rand = Math.random();
        if (rand<0.3) {
          LED1.set();
          led = 1;
        } else if (rand>0.6) {
          LED3.set();
          led = 3;
        } else {
          LED2.set();
          led = 2;
          stoptimeout = setTimeout(function() {
          LED2.reset();
            console.log("Good! - Press to continue.");
            led = 0;
          }, 1000);
        }
        inittime = getTime();
        ready = 1;
      }
    }, (3+Math.random()*5)*1000);
  } else {
    if (doublepress == 1)
      console.log("Press again.");
    else
      console.log("Press when you are ready!");
  }

}, BTN, {edge:"rising", debounce:debouncetime, repeat:true});
console.log("Press when you are ready!");